using System.Collections;
using System.Collections.Generic;
using UnityEngine;
///<summary>
///Este codigo debe de estar en una camara diferente a la principal de otra forma la camara no para de moverse.
///</summary>
public class CameraController : SteeringBehaviors
{
    public float MaxDistance
    {
        get
        {
            return maxDistance;
        }
        set
        {
            maxDistance = value;
        }
    }
    public float SeekingBehaviourDistance
    {
        get
        {
            return seekingBehaviourDistance;
        }
        set
        {
            seekingBehaviourDistance = value;
        }
    }
    public float ArrivingBehaviourDistance
    {
        get
        {
            return arrivingBehaviourDistance;
        }
        set
        {
            arrivingBehaviourDistance = value;
        }
    }
    public float StopMovingDistance
    {
        get
        {
            return stopMovingDistance;
        }
        set
        {
            stopMovingDistance = value;
        }
    }
    public string CameraControllerbutton
    {
        get
        {
            return cameraControllerButton;
        }
        set
        {
            cameraControllerButton = value;
        }
    }
    public GameObject ReferenceObject
    {
        get
        {
            return referenceObject;
        }
        set
        {
            referenceObject = value;
        }
    }
    public Camera ReferenceCamera
    {
        get
        {
            return referenceCamera;
        }
        set
        {
            referenceCamera = value;
        }
    }
    ///<summary>
    ///Determina qu� tan lejos puede estar la c�mara del objeto de referencia.
    ///</summary>
    [SerializeField]
    private float maxDistance;
    ///<summary>
    ///Determina cuando el comportamiento de �Seguir� se activar�, el comportamiento es activado si la distancia entre el objeto y su objetivo es mayor o igual a este valor.
    ///</summary>
    [SerializeField]
    private float seekingBehaviourDistance;
    ///<summary>
    ///Determina cuando el comportamiento de �Llegada� se activar�, el comportamiento es activado si la distancia entre el objeto y su objetivo es menor a este valor.
    ///</summary>
    [SerializeField]
    private float arrivingBehaviourDistance;
    ///<summary>
    ///Determina cuando el objeto debe de parar de moverse, el objeto deja de moverse si la distancia entre el objeto y su objetivo es menor a este valor.
    ///</summary>
    [SerializeField]
    private float stopMovingDistance;
    ///<summary>
    ///Determina el bot�n que activar� el comportamiento del objeto (que mueva la c�mara), utiliza la nomenclatura de botones de Unity.
    ///</summary>
    [SerializeField]
    private string cameraControllerButton;
    ///<summary>
    ///Determina la posici�n original de la c�mara, la posici�n original de la c�mara ser� la misma que la posici�n de este objeto.
    ///</summary>
    [SerializeField]
    private GameObject referenceObject;
    ///<summary>
    ///Determina la posici�n del cursor dentro del mundo del juego.
    ///La camara de referencia deve de ser ortografica y su display objetivo deve de ser menor que el de la camara con este codigo.
    ///</summary>
    [SerializeField]
    private Camera referenceCamera;

    ///<summary>
    ///Es la distancia que hay entre el objetivo y la posici�n actual de la camara.
    ///</summary>
    private float goingDistance;
    ///<summary>
    ///Determina si la camara esta llendo a su objetivo.
    ///</summary>
    private bool going = false;
    ///<summary>
    ///Determina si la camara llego a su objetivo.
    ///</summary>
    private bool arrived = false;
    ///<summary>
    ///Determina si la camara esta regresando a su posici�n original.
    ///</summary>
    private bool returning = false;
    ///<summary>
    ///Es un vector temporal que se utiliza como referencia al objetivo de la camara.
    ///</summary>
    private Vector3 tmpVector;

    ///<summary>
    ///Cuando se preciona cameraControllerButton, determina el objetivo de la camara y se llama a la funcion StateMachine.
    ///</summary>
    private void Update()
    { 
        this.distance = CalculateDistance(transform.position, referenceObject.transform.position);
        if (Input.GetButton(cameraControllerButton))
        {
            returning = false;
            Vector3 pos = referenceCamera.ScreenToWorldPoint(Input.mousePosition);
            float distance2 = Mathf.Sqrt(Mathf.Pow(referenceObject.transform.position.x - pos.x, 2) + Mathf.Pow(referenceObject.transform.position.y - pos.y, 2));
            //Si la distancia entre la camara y la posici�n del cursor dentro del mundo del juego es mayor a la distancia maxima se crea un punto en la distancia maxDistance dentro
            //de la recta imaginaria entre la camara y la posici�n del cursor dentro del mundo del juego, este punto es utilizado como el objetivo de la camara.
            if (distance2 > maxDistance)
            {
                float x = distance2 / maxDistance;
                Vector2 tmpVector2 = new Vector2((referenceObject.transform.position.x * -1) + pos.x, (referenceObject.transform.position.y * -1) + pos.y);
                pos = new Vector2(tmpVector2.x/x + referenceObject.transform.position.x, tmpVector2.y/x + referenceObject.transform.position.y);
            }
            if (!arrived)
            {
                going = true;
                tmpVector = new Vector3(pos.x, pos.y, transform.position.z);
                goingDistance = CalculateDistance(transform.position, tmpVector);
            }
            else
            {
                transform.position = new Vector3(pos.x, pos.y, transform.position.z);
            }
        }
        if (Input.GetButtonUp(cameraControllerButton))
        {
            returning = true;
            going = false;
            arrived = false;
        }
        StateMachine();
    }
    ///<summary>
    ///Esta funcion cambia el "steering behavior" (el como se mueve la camara) dependiendo de distancia que existe entre la misma y su objetivo (cuando esta llendo al punto objetivo) 
    ///o su posici�n original (si esta rtegresando a s upocicion original) y el estado de las variables going y returning.
    ///</summary>
    private void StateMachine()
    {
        //Si going es verdadero la camara se mueve hacia su objetivo
        if (going)
        {
            if (goingDistance < stopMovingDistance)
            {
                arrived = true;
                going = false;
                transform.position = new Vector3(tmpVector.x, tmpVector.y, transform.position.z);
            }
            else if (goingDistance < arrivingBehaviourDistance)
            {
                this.acceleration += arrival(tmpVector, vectorPosition, velocity, MaxSpeed);
            }
            else if (goingDistance >= seekingBehaviourDistance)
            {
                this.acceleration += seek(tmpVector, vectorPosition, velocity, MaxSpeed, MaxForce);
            }
            AplySteering();
        }
        //Si returning es verdadero la camara se mueve hacia su posici�n original.
        if (returning)
        {
            if (returning && this.distance < stopMovingDistance)
            {
                returning = false;
                transform.position = new Vector3(referenceObject.transform.position.x, referenceObject.transform.position.y, transform.position.z);
            }
            if (returning && this.distance < arrivingBehaviourDistance)
            {
                this.acceleration += arrival(referenceObject.transform.position, vectorPosition, velocity, MaxSpeed);
            }
            else if (returning && this.distance >= seekingBehaviourDistance)
            {
                this.acceleration += seek(referenceObject.transform.position, vectorPosition, velocity, MaxSpeed, MaxForce);
            }
            AplySteering();
        }
    }
}
